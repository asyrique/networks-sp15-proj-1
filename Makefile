# C compiler to use. Use gcc for C and g++ for C++
CC = gcc

# Compiler flags
# -g    output error info for gdb
# -Wall turns on almost all compiler warnings
CFLAGS = -g -Wall

all: sircd

sircd: sircd.c
	mkdir -p ./bin
	$(CC) $(CFLAGS) -o ./bin/sircd sircd.c

run: sircd
	./bin/sircd

clean:
	$(RM) ./bin/sircd
